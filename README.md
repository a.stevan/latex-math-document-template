# latex-math-document-template
A template to write simple math documents.

## Writing a new document
- fork this repository
- modify the `document.tex` file

## Compile the document
- make sure to install the `latexmk` package and command
- run `make document`

> :bulb: **Note**  
> the local repo can be cleaned from compilation files with `make clean`

## The document template
![document.pdf](document.pdf)
