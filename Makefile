document:
	latexmk document.tex

clean:
	find -type f -name *.aux -exec rm {} \;
	find -type f -name *.fdb_latexmk -exec rm {} \;
	find -type f -name *.fls -exec rm {} \;
	find -type f -name *.log -exec rm {} \;
